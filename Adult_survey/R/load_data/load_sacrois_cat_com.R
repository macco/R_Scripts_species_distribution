if (file.exists(paste0(path_tidy_sacrois_data, 
                       "/df_sacrois_cat_com_cet_sol",
                       min(vec_year), "_",
                       max(vec_year), ".rds"))) {
  
  df_sacrois_cat_com_cet_sol <- read_rds(paste0(path_tidy_sacrois_data, 
                                                "/df_sacrois_cat_com_cet_sol",
                                                min(vec_year), "_",
                                                max(vec_year), ".rds"))
  
  df_sacrois_cat_com_sol <- read_rds(paste0(path_tidy_sacrois_data, 
                                            "/df_sacrois_cat_com_sol",
                                            min(vec_year), "_",
                                            max(vec_year), ".rds"))
} else {
  
  ### load Cat com for sole only
  source("R/load_data/f_load_sacrois_sol.R")
  
  df_sacrois_cat_com_sol <- map_dfr(.x = vec_year,
                                    load_sacrois_sol)
  
  saveRDS(df_sacrois_cat_com_sol, file = paste0(path_tidy_sacrois_data, 
                                                "/df_sacrois_cat_com_sol",
                                                min(vec_year), "_",
                                                max(vec_year), ".rds"))
  
  ### load Cat com for sole with CET catch only   
  source("R/load_data/f_load_sacrois_cet_sol.R")
  
  df_sacrois_cat_com_cet_sol <- map_dfr(.x = vec_year,
                                        load_sacrois_cet_sol)
  
  saveRDS(df_sacrois_cat_com_cet_sol, file = paste0(path_tidy_sacrois_data, 
                                                    "/df_sacrois_cat_com_cet_sol",
                                                    min(vec_year), "_",
                                                    max(vec_year), ".rds"))
  
}