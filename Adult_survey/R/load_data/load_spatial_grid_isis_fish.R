################################################################################
### Load MACCO Isis-Fish spatial grid
################################################################################

### load Rdata file of isis-fish spatial grid for macco
load("data/tidy/isis_fish_grid/ISISgrid16.RData")

### extract polygons and raster them
isis_grid_raster <- raster(ISISgrid$GRIDspatialpoly,
                           ncol = 28, nrow = 26 * 2) #ncell=728
projection(isis_grid_raster) <- "+proj=longlat +datum=WGS84 +no_defs"

### set as sf object
sf_isis_grid <- rasterToPolygons(isis_grid_raster) %>%
  st_as_sf(gridpolygon) %>%
  mutate(square = as_factor(row_number())) %>%
  dplyr::select(-layer) 

filter_sf <- function(.data, xmin = NULL, xmax = NULL, ymin = NULL, ymax = NULL) {
  bb <- sf::st_bbox(.data)
  if (!is.null(xmin)) bb["xmin"] <- xmin
  if (!is.null(xmax)) bb["xmax"] <- xmax
  if (!is.null(ymin)) bb["ymin"] <- ymin
  if (!is.null(ymax)) bb["ymax"] <- ymax
  sf::st_filter(.data, sf::st_as_sfc(bb), .predicate = sf::st_within)
}

# 
# 

# 
# ggplot() +
#   # geom_sf(data = filt_bbox) +
#   geom_sf(data = isis_grid_sf)


