```{r plot_table_package, echo = FALSE}

library("tidyverse")

### plot and tables
library("gridExtra")
library("patchwork")
library("pander")
library("captioner")
library("kableExtra")
library("flextable")

###-----------------------------------------------------------------------------
### making map plots
library("mapdata")

##-----------------------------------------------------------------------------
## ggplot2 option
library("ggthemr")
# 
cb.grey.pal = c(
  "#E69F00",  # Gold,
  # "#434FF0", 
  # "#187AAC",  # ifremer blue
  "#0092D2",
  "#F0E442",  # yellow
  # "#FFFF00",  # yellow
  "#D55E00",  # Red
  "#009E73",  # Green,
  "#CC79A7",  # Violet
  "#56B4E9",  # Light blue,
  "#999999",  # grey
  "#0072B2",  # dark blue
  "#F3E816",  # yellow ifremer  
  "#332288",  # violet
  "#AA4499",  # pink
  "#882255",  # other pink
  "#661100",  # red
  "#117733",  # green
  "#000000"   # black
)
# 
cb_grey_pal <- define_palette(
  swatch = cb.grey.pal,
  gradient = c(lower = cb.grey.pal[1L], upper = cb.grey.pal[2L])
)

ggthemr(cb_grey_pal)

theme_set(theme_classic(base_size = 18))

theme_update(strip.text = element_text(size = 10, lineheight = 0.1))

```
