################################################################################
### render document
################################################################################


talk_to_render <- "talk_obsmer"

rmarkdown::render(input = here("R", "dynamic_document",
                               talk_to_render,
                               paste0(talk_to_render, ".Rmd")),
                  "xaringan::moon_reader")


# xaringan::inf_mr(here(assessment_year,
#                       talk_to_render,
#                       paste0(talk_to_render, ".Rmd")))


###-----------------------------------------------------------------------------
### render Rmd file in doc directory
# xaringanBuilder::build_pdf(input = here(assessment_year,
#                                         talk_to_render,
#                                         paste0(talk_to_render, ".html")))

###-----------------------------------------------------------------------------
### render Rmd file in doc directory
source("R/fun/utils/xaringan_to_pdf.R")
xaringan_to_pdf(here("R", "dynamic_document",
                     talk_to_render,
                     paste0(talk_to_render, ".html")))
