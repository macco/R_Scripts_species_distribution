################################################################################
### plot obsvente length class
################################################################################


boxplot_obsvente_lenclass <- ggplot(df_obsmer, aes(x = catchCat, y = lenCls, fill = spp)) + 
  geom_boxplot() +
  xlab("Catch category") +
  ylab("Length class")


