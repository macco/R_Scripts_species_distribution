load("data/raw/obsmer/base_obsmer_non_validee_a_partir_01012021_COST_12_06_21.Rdata")



df_hh <- COST@hh %>%
  filter(area %in% c("27.8.a", "27.8.b", "27.8.c"))

###-----------------------------------------------------------------------------
df_macco_spp <- COST@hl %>%
  left_join(df_hh, .) %>%
  filter(spp %in% species_vec) %>%
  group_by(spp) %>%
  mutate(min_lenCls = min(lenCls, na.rm = TRUE))


###-----------------------------------------------------------------------------
dfs_macco_spp <- df_macco_spp %>%
  dplyr::select(spp, min_lenCls) %>%
  distinct()

macco_hist_obsmer_length <- ggplot(data = df_macco_spp, aes(x = lenCls)) + 
  geom_histogram(bins = 50) +
  # geom_vline(data = df_lophi, aes(xintercept = min_lenCls)) +
  ggtitle(paste("Obsmer")) +
  facet_wrap(~spp, scales = "free", ncol = 2) +
  xlab("Length class") + ylab("Number")

f_save_plot(plot = macco_hist_obsmer_length,
            file_name = "macco_hist_obsmer_length",
            path = path_figure,
            save = TRUE,
            width = 29,
            height = 21,
            device = 'pdf',
            units = 'cm')


###-----------------------------------------------------------------------------
df_macco_raj <- COST@hl  %>%
  left_join(df_hh, .)%>%
  
  filter(grepl("raj|Raj",spp)) %>%
  group_by(spp) %>%
  mutate(min_lenCls = min(lenCls, na.rm = TRUE))

macco_hist_obsmer_length_raj <- ggplot(data = df_macco_raj, aes(x = lenCls)) + 
  geom_histogram(bins = 50) +
  # geom_vline(data = df_lophi, aes(xintercept = min_lenCls)) +
  ggtitle(paste("Obsmer")) +
  facet_wrap(~spp, scales = "free", ncol = 2) +
  xlab("Length class") + ylab("Number")

f_save_plot(plot = macco_hist_obsmer_length_raj,
            file_name = "macco_hist_obsmer_length_raj",
            path = path_figure,
            save = TRUE,
            width = 29,
            height = 21,
            device = 'pdf',
            units = 'cm')

###-----------------------------------------------------------------------------
dfs_macco_raj <- df_macco_raj %>%
  group_by(spp) %>%
  summarise(min_lenCls = min(lenCls, na.rm = TRUE))


