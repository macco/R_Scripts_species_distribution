# f_map_obsmer_maturity <- function(df_data) {
#   
#   sf_obsmer_square <- select(sf_obsmer_month_isis,
#                              PIDSID, geometry) %>%
#     distinct()
#   
#   plot_empty_map +
#     geom_sf(data = sf_obsmer_square,size = 0.001) +
#     geom_sf(data = df_data, aes(fill = "Presence"), size = 0.001) +
#     # geom_sf(data = sf_ices_rec, aes(colour = "red"), size = 1) +
#     scale_fill_viridis_d(
#     #   # trans = "log",
#     #   # limits = c(0.001, round(max(df_data$quantity_kg)+51, digits = -2)),
#       name = "") +
#     facet_grid( quarter ~ maturity) 
# }
# 
# 
# sf_obsmer_month_isis %>%
#   split(.$spp) %>%
#   purrr::map(f_map_obsmer_maturity) -> map_obsmer_maturity 
# 
# map_obsmer_maturity$`Lepidorhombus whiffiagonis`
# map_obsmer_maturity$`Solea solea`
# map_obsmer_maturity$`Lophius budegassa`
# map_obsmer_maturity$`Lophius piscatorius`
# 
# 
# 
# map2(.x = map_obsmer_maturity,
#      .y = names(map_obsmer_maturity),
#      ~ f_save_plot(plot = .x,
#                    file_name = paste0("map_obsmer_maturity", .y),
#                    path = path_figure_macco,
#                    save = TRUE,
#                    width = 29,
#                    height = 21,
#                    device = 'pdf',
#                    units = 'cm'))
# 
# f_map_obsmer_maturity_stage <- function(df_data) {
#   
#   sf_obsmer_square <- select(sf_obsmer_month_isis,
#                              PIDSID, geometry) %>%
#     distinct()
#   
#   plot_empty_map +
#     geom_sf(data = sf_obsmer_square, size = 0.001) +
#     geom_sf(data = df_data, aes(fill = mean_n), size = 0.001) +
#     # geom_sf(data = sf_ices_rec, aes(colour = "red"), size = 1) +
#     scale_fill_viridis_c(
#       # trans = "log",
#       # limits = c(0.001, round(max(df_data$quantity_kg)+51, digits = -2)),
#       name = "Nombre") +
#     facet_wrap(  ~ maturity) 
# }
# 
# 
# sf_obsmer_month_isis %>%
#   mutate(spp_maturity = paste0(spp, " ", maturity)) %>%
#   split(.$spp_maturity) %>%
#   purrr::map(f_map_obsmer_maturity_stage) -> map_obsmer_maturity_stage 
# 
# map_obsmer_maturity_stage$`Lepidorhombus whiffiagonis Imature`
# map_obsmer_maturity_stage$`Solea solea Mature`
# map_obsmer_maturity_stage$`Lophius budegassa`
# map_obsmer_maturity_stage$`Lophius piscatorius`
# 
# 
# 
# map2(.x = map_obsmer_maturity,
#      .y = names(map_obsmer_maturity),
#      ~ f_save_plot(plot = .x,
#                    file_name = paste0("map_obsmer_maturity", .y),
#                    path = path_figure_macco,
#                    save = TRUE,
#                    width = 29,
#                    height = 21,
#                    device = 'pdf',
#                    units = 'cm'))

###-----------------------------------------------------------------------------
###
###-----------------------------------------------------------------------------

f_map_obsmer_maturity_quarter <- function(df_data, grid, coast_line = NULL) {
  #f_map_obsmer_maturity_quarter <- function(df_data, grid, coast_line = NULL) {
    
  # sf_grid  <- sf_grid %>%
  #   filter(quarter %in% df_data$quarter)
  
  if (is.null(coast_line)) {
    plot_empty_map <- Biscay_Gulf_map_custom_whitout  
  }
  plot_empty_map +
    geom_sf(data = grid, size = 0.001) +
    geom_sf(data = df_data, aes(fill = mean_presence), size = 0.001) +
    scale_fill_viridis_c(limits = c(0, 1),
                         breaks = seq(0, 1, 0.5),
                        name = "Presence moyenne") +
    facet_wrap(  ~ maturity) +
    ggtitle(df_data$spp)
}


sf_obsmer_quarter_isis %>%
  ## AJOUT AU CODE POUR RECUPERER QUE LES JUVENILES 
  mutate(maturity = ifelse(maturity == "Immature", "Juvenile", ifelse(maturity == "Mature", "Adulte", maturity))) %>%
  ## ICI FILTER
  filter(maturity == "Adulte") %>%
  # Retour au code normal 
  mutate(spp_maturity = paste0(spp, " ", quarter)) %>%
  split(.$spp_maturity) %>%
  purrr::map(f_map_obsmer_maturity_quarter, grid = sf_isis_grid_obsmer,
             coast_line = NULL) -> map_obsmer_maturity_quarter

map_obsmer_maturity_quarter$`Lepidorhombus whiffiagonis 1`
map_obsmer_maturity_quarter$`Solea solea 2`
map_obsmer_maturity_quarter$`Solea solea 3`
map_obsmer_maturity_quarter$`Solea solea 4`

map2(.x = map_obsmer_maturity_quarter,
     .y = names(map_obsmer_maturity_quarter),
     ~ f_save_plot(plot = .x,
                   file_name = paste0("map_obsmer_maturity_quarter", .y),
                   path = path_figure_macco,
                   save = TRUE,
                   width = 29,
                   height = 21,
                   device = 'png',
                   units = 'cm'))



saveRDS(map_obsmer_maturity_quarter, 
        file = here(path_data_tidy_plots, "map_obsmer_maturity_quarter.rds"))
