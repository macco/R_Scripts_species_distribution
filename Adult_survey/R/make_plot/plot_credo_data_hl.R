################################################################################
### plot credo data : length structure
################################################################################
n_col_facet_wrap <- 1

###-----------------------------------------------------------------------------
### all data
plot_cs_hl_all <- ggplot() +
  geom_bar(data = dfs_cs_hl, aes(x = lenCls, y = prop_fish),
           stat="identity") +
  facet_wrap(. ~ year, ncol = n_col_facet_wrap) + 
  xlab("Length class") + ylab("Proportion of fish")


JBUtilities::show_ggplot2(plot_cs_hl_all)
JBUtilities::save_plot(plot_cs_hl_all, path = "figures")

###-----------------------------------------------------------------------------
### cotiniere only
plot_cs_hl_cotiniere <- ggplot() +
  geom_bar(data = dfs_cs_hl_cotiniere, aes(x = lenCls, y = prop_fish),
           fill = "blue",
           stat="identity") +
  facet_wrap(. ~ year, ncol = n_col_facet_wrap) + 
  xlab("Length class") + ylab("Proportion of fish")


JBUtilities::show_ggplot2(plot_cs_hl_cotiniere)
JBUtilities::save_plot(plot_cs_hl_cotiniere, path = "figures")

###-----------------------------------------------------------------------------
### all data and without cotiniere
plot_cs_hl_all_no_cotiniere <- ggplot() +
  geom_bar(data = dfs_cs_hl_all_cotiniere,
           aes(x = lenCls, y = prop_fish, fill = data),
           stat="identity", position = "dodge") +
  facet_wrap(. ~ year, ncol = n_col_facet_wrap) + 
  xlab("Length class") + ylab("Proportion of fish") +
  xlim(200, 500)  +
  theme(legend.title=element_blank(),
          legend.position = "bottom")

JBUtilities::show_ggplot2(plot_cs_hl_all_no_cotiniere)
JBUtilities::save_plot(plot_cs_hl_all_no_cotiniere, path = "figures")

###-----------------------------------------------------------------------------
### all data and without cotiniere
n_col_facet_wrap <- 2

plot_cs_hl_all_no_cotiniere_diff <- ggplot() +
  geom_bar(data = dfs_cs_hl_all_cotiniere_diff,
           aes(x = lenCls, y = diff_prop),
           stat="identity", position = "dodge") +
  facet_wrap(. ~ year, ncol = n_col_facet_wrap) + 
  xlab("Length class") + ylab("Proportion of fish") +
  xlim(200, 500)  +
  theme(legend.title=element_blank(),
        legend.position = "bottom")

JBUtilities::show_ggplot2(plot_cs_hl_all_no_cotiniere_diff)
JBUtilities::save_plot(plot_cs_hl_all_no_cotiniere_diff, path = "figures")

