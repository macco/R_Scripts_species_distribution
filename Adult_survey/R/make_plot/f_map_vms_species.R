f_map_vms_species_kg <- function(year, species) {
  
  sf_sacrois_cet_sol_month_rec <- sf_sacrois_cet_sol_month_rec %>%
    dplyr::filter(year == year, esp_cod_fao   == species)
  
  # plot_empty_map +
    ggplot() +
    geom_sf(data = sf_sacrois_cet_sol_month_rec, aes(fill = quantity_t_fct ), size = 0.001) +
      geom_sf_text(data = sf_sacrois_cet_sol_month_rec, aes(label = sect_cod_sacrois_niv5), size = 1, colour = "white") + 
    # geom_sf(data = sf_ices_rec, aes(colour = "red"), size = 1) +
    scale_fill_viridis_d(
    #   # trans = "log",
    #   # limits = c(0.001, round(max(sf_sacrois_cet_sol_month_rec$quantity_kg)+51, digits = -2)),
    #   # name = "Effort"
      ) +
      # scale_fill_fermenter() +
    facet_wrap( ~ month, ncol = 4) +
    ggtitle(paste0("Quantity in kg of species : ", sf_sacrois_cet_sol_month_rec$esp_lib_fra, " in ", year))
}

f_map_vms_species_euro <- function(year, species) {
  
  sf_sacrois_cet_sol_month_rec <- sf_sacrois_cet_sol_month_rec %>%
    filter(year == year, espece_fao_cod  == species)
  
  plot_empty_map +
    geom_sf(data = sf_sacrois_cet_sol_month_rec, aes(fill = value_euro ), size = 0.001) +
    scale_fill_viridis_c(
      # trans = "log",
      # limits = c(0.001, round(max(sf_sacrois_cet_sol_month_rec$quantity_kg)+51, digits = -2)),
      name = "Effort") +
    facet_wrap( ~ month, ncol = 3) +
    ggtitle(paste0("Value in euro of species : ", sf_sacrois_cet_sol_month_rec$espcece_fao_lib))
}

