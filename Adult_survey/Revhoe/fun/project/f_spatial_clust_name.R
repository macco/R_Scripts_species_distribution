
f_spatial_clust_name <- function(df_data, var_to_fill) {
  return(unique(paste0(df_data$Species,
                       "_",
                       df_data$stade,
                       "_",
                       var_to_fill,
                       "_month",
                       unique(df_data$month))))
}