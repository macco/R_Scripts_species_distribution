

### load data files
captur <- read.csv("dataevhoe/raw/survey/evohe/Captures.csv",
                   h = T, sep = ";", dec = ".")
trait <- read.csv("dataevhoe/raw/survey/evohe/Traits.csv",
                  h = T, sep = ";", dec = ".")
tail <- read.csv("dataevhoe/raw/survey/evohe/Tailles.csv",
                 h = T, sep = ";", dec = ".")

capt <- merge(captur, trait[,-c(1:2)], by = "Trait")
datev <- as_tibble(merge(capt[, -c(5:6)], tail[, -c(1:2)],
                         by = c("Trait","Espece")))  %>%
  filter(Espece %in% lst)
rm(capt)

###-----------------------------------------------------------------------------
# ogive de maturité
load("dataevhoe/raw/survey/evohe/MATURITYresults.Rdata")
og <- NULL


for (NAME in lst) {
  temp <- data.frame(OGIVEres[[NAME]]$ALL$DistribClass)
  temp$species <- rep(NAME,times = nrow(temp))
  og <- as_tibble(rbind(og,temp))
}

### load  ogive maturity results and merge with evohe data
datatt <- left_join(datev, og, 
                    by = c("Espece" = "species", "Longueur" = "LT")) 

### load summary of ogive maturity analysis and merge with evohe data
load("dataevhoe/raw/survey/evohe/MATURITYsummary.Rdata")
df_maturity_summary <- TABLEmaturity %>%
  as_tibble() %>%
  filter(Species %in% lst) %>%
  mutate(across(starts_with("L"), as.numeric),
         across(contains("median"), as.numeric),
         across(where(is.character), as.factor))


datas <- left_join(datatt, df_maturity_summary,
                   by = c("Espece" = "Species")) %>%
  filter(!is.na(L50median)) %>%
  mutate(Matprop = Nombre * MATprop  , IMMatprop = Nombre * 1 - MATprop) %>% 
  dplyr::select(Campagne, Annee, Mois, Long, Lat, Espece, Longueur, Nombre, L50median, IMMatprop, Matprop) %>% 
  tidyr::gather(Maturity, NbMat, Matprop:IMMatprop) %>% 
  mutate(stade = case_when(Longueur <= L50median ~ "Juv",
                           Longueur > L50median ~ "Adu",
                           TRUE ~ "OTH")) %>%
  group_by(Campagne, Annee, Mois, Long, Lat, Espece, stade) %>%
  summarise(Nombre = sum(Nombre)) %>%
  mutate(Mois = replace(Mois, Mois==9 | Mois==10 | Mois==12 , 11))  %>% # ne garder que les mois en hiver (comme une seule grande campagne)
  ### AJOUT 
  filter(Mois == 11)