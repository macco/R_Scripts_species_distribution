################################################################################
### Load MACCO Isis-Fish spatial grid
################################################################################

### load Rdata file of isis-fish spatial grid for macco
load("dataevhoe/tidy/isis_fish_grid/ISISgrid16.RData")

### extract polygons and raster them
isis_grid_raster <- raster(ISISgrid$GRIDspatialpoly,
                           ncol = 28, nrow = 26 * 2) #ncell=728
projection(isis_grid_raster) <- "+proj=longlat +datum=WGS84 +no_defs"

### set as sf object
isis_grid_sf <- rasterToPolygons(isis_grid_raster) %>%
  st_as_sf(gridpolygon) %>%
  mutate(square = as_factor(row_number()),
         layer =   as_factor(row_number()))
