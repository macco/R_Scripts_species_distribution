################################################################################
### Clust geo
################################################################################

###-----------------------------------------------------------------------------
### load function
source("R/fun/project/f_spatial_clust.R")

###-----------------------------------------------------------------------------
### choose variable to plot and grouping variable
var_spatial_clust <- "S_x"
var_to_group <- c("year", "month")

if (file.exists(paste0("list_spatial_clust_",
                      var_spatial_clust,
                      species_name, "_",
                      min(year_vec), "_",
                      max(year_vec), "_",
                      "month_vec",
                      ".rds"))) {
  
  list_spatial_clust_month_species <- read_rds(paste0(path_tidy_data,
                                                      "/list_spatial_clust_",
                                                      var_spatial_clust,
                                                      species_name, "_",
                                                      min(year_vec), "_",
                                                      max(year_vec), "_",
                                                      "month_vec",
                                                      ".rds"))
  
} else {
  ###-----------------------------------------------------------------------------
  ### run clustering
  list_spatial_clust_month_species  <- S_x_df_poly %>%
    group_by(across(all_of(var_to_group))) %>%
    group_split() %>%
    purrr::map(~ f_spatial_clust(df_spatial_clust = .x,
                                 spatial_grid = loc_poly_sf_2,
                                 var_spatial_clust = var_spatial_clust,
                                 n_cluster = 5,
                                 range_alpha = seq(0.2, 0.8, 0.1)))
  
  ###-----------------------------------------------------------------------------
  ### set name to list element
  names(list_spatial_clust_month_species) <- S_x_df_poly %>%
    group_by(across(all_of(var_to_group))) %>%
    group_split() %>%
    purrr::map(~ f_spatial_clust_name(df_data = .x,
                                      var_to_fill = var_spatial_clust))
  
  ###-----------------------------------------------------------------------------
  ### save as rds
  saveRDS(list_spatial_clust_month_species, 
          file = paste0(path_tidy_data,
                        "/list_spatial_clust_",
                        var_spatial_clust,
                        species_name, "_",
                        min(year_vec), "_",
                        max(year_vec), "_",
                        "month_vec",
                        ".rds"))
}