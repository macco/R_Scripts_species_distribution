################################################################################
### Clust geo test
################################################################################
library("ClustGeo")

dat <- S_x_df_poly %>%
  sf::st_drop_geometry() %>%
  dplyr::select(S_x) 
  
head(dat)

### no spatial constraints
D0 <- dist(dat) # the socio-economic distances
tree <- hclustgeo(D0)

###-----------------------------------------------------------------------------
### compute spatial distance from points to points
# matrix_geo_distance <- st_distance(S_x_df, S_x_df)
# D1 <- matrix_geo_distance
# 
# ### Choice of the mixing parameter alpha
# range.alpha <- seq(0, 1, 0.1)
# K <- 5
# cr <- choicealpha(D0, D1,
#                   range.alpha, 
#                   K, graph = FALSE)
# cr$Q # proportion of explained inertia
# 
# ### Modified partition obtained with alpha=0.2
# tree <- hclustgeo(D0, D1, alpha = 0.2)
# P5bis <- cutree(tree,5)
# 
# # The modified partition P5bis is visualized on the map.
# 
# sp::plot(map, border = "grey", col = P5bis, 
#          main = "Partition P5bis obtained with alpha=0.2 
#          and geographical distances")
# legend("topleft", legend = paste("cluster",1:5), 
#        fill = 1:5, bty = "n", border = "white")

###-----------------------------------------------------------------------------
### Change the partition to take neighborhood constraint into account
library("spdep")
# ?poly2nb
list.nb <- poly2nb(S_x_df_poly) #list of neighbors of each city
# ?nb2mat
A <- nb2mat(list.nb, style = "B")
diag(A) <- 1
# colnames(A) <- rownames(A) <- city_label

# The dissimilarity matrix D1 is then 1 minus A.
D1 <- as.dist(1 - A)

###-----------------------------------------------------------------------------
### Choice of the mixing parameter alpha
### The procedure for the choice of alpha is repeated here with the new matrix D1.

range.alpha <- seq(0, 1, 0.1)
K <- 5
cr <- choicealpha(D0, D1, range.alpha,
                  K, graph = FALSE)
plot(cr)

# normalized proportion of explained inertia
cr$Qnorm 
plot(cr, norm = TRUE)

set_alpha <- 0.4

### Modified partition obtained with alpha
tree <- hclustgeo(D0, D1, alpha = set_alpha)
P5ter <- cutree(tree, K)


S_x_df_poly <- S_x_df_poly %>%
  mutate(clustering = as.factor(P5ter))

ggplot() + 
  geom_sf(data = S_x_df_poly, aes(fill = clustering))
