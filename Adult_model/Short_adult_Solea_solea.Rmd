---
title: "Solea solea adult maps"
subtitle: "IFREMER - MACCO Project - https://www.macco.fr/"
author: "Vajas P., Ricouard A., Lecomte J-B., Brind'Amour A., Laffargue P., Mahevas S."
date: ' `r Sys.Date()` '
output:
  html_document: 
    toc: true
    toc_float: true
    number_sections: true
editor_options: 
  chunk_output_type: console
#bibliography: BiblioThese.bib
#link-citations: true
#csl: journal-of-applied-ecology.csl
---


```{r setup, include=FALSE, cache=FALSE}
knitr::opts_chunk$set(fig.width=12, fig.height=8, fig.path='Figs/',
                      echo=FALSE, warning=FALSE, message=FALSE) #, cache = TRUE)
#options(device = "quartz")
```



# Header *Solea solea*

<br>
<br>

The *Solea solea* data are derived from the model output developed in the paper *"Alglave, B., Rivot, E., Etienne, M. P., Woillez, M., Thorson, J. T., & Vermard, Y. (2022). Combining scientific survey and commercial catch data to map fish distribution. ICES Journal of Marine Science, 79(4), 1133-1149"* (https://doi.org/10.1093/icesjms/fsac032). The latent density variable is called `S_x` and can be studied at different time scales (months, years) and spatial scales (1/16 of the statistical rectangle). In this report we present the mean of S_x, then the results of clustering at the level of months, years, then months x years. For further methodological details, please refer to the reference document at the root of the files. Package notable, `tidyverse`, `sf`, `ClustGeo`. 


<br>
<br>

# Mean map *Solea solea*

<br>
<br>

```{r include=FALSE, echo=FALSE, warning=FALSE}

### Charge des fichiers pour la génération future des cartes 

# Variables de controles 

species_name <- "Solea solea"

year_vec <- c(2008:2018)

month_vec <- (1:12)

# Boot des script R

source("R/fun/boot.R")

source("R/load_data/load_algave_model_output.R")

source("R/load_data/make_algave_spatial_grid_16.R")

# Ici changer S_x_df

ab_df <- S_x_df %>% group_by(year, month, t) %>% summarise(Ab = sum(S_x))
S_x_df <- inner_join(S_x_df, ab_df)

S_x_df$year <- as.numeric(as.character(S_x_df$year))
S_x_df$month <- as.integer(as.character(S_x_df$month))

S_x_df <- S_x_df %>% mutate(month = sprintf("%02d", month)) %>%
 mutate(Year_Month = paste0(year,"_",month)) %>% 
   mutate(month = month  %>% as.numeric) %>%
 mutate(month = month  %>% as.factor() %>% factor(levels = c(1:12)))

# Maintenant le script fonctionne

S_x_df_mean <- S_x_df %>% group_by(key, year) %>% summarise(S_x = mean(S_x), Ab = mean(Ab))
S_x_df_mean_2 <- S_x_df_mean %>% group_by(key) %>% summarise(S_x = mean(S_x), Ab = mean(Ab))
S_x_df_unique_key <- S_x_df %>% as.data.frame() %>% dplyr::select(-c("month", "year", "t", "S_x", "Year_Month", "Ab")) %>% unique()
S_x_df_new_table <- left_join(S_x_df_unique_key, S_x_df_mean_2, by = "key")

S_x_df_new_table$year <- 1
S_x_df_new_table$month <- 1

S_x_df <- S_x_df_new_table

# Boot n°2 

source("R/load_data/filter_spatial_grid.R")

source("R/load_data/join_model_spatial_grid_16.R")

source("R/fun/project/f_spatial_clust.R")


```

```{r include=TRUE, echo=FALSE, warning=FALSE}

library(ggspatial)

Biscay_Gulf_map <- read_sf("Biscay_Gulf_map.shp")
#geom_sf(data = Biscay_Gulf_map)

Biscay_Gulf_map_custom <-  ggplot() +
  geom_sf(data = Biscay_Gulf_map) +  
  # Ajout d'une échelle de taille
  annotation_scale(location = "br", width_hint = 0.2) +
  # Ajout d'une flèche pour indiquer le nord
  annotation_north_arrow(location = "tl", which_north = "true", 
                         pad_x = unit(0.05, "in"), pad_y = unit(0.05, "in"),
                         style = north_arrow_fancy_orienteering) + 
  theme(
    # Couleur du fond
    panel.background = element_rect(fill = "white", colour = "black"),
    # Couleur des lignes de grille
    panel.grid.major = element_line(colour = "transparent"),
    # Couleur du fond du graphique
    plot.background = element_rect(fill = "white", colour = "white"),
    # Couleur du fond de la légende
    legend.background = element_rect(fill = "white", colour = "white"),
    # Position de la légende
    legend.position = "bottom"
  ) +
  xlab('Longitude') +
  ylab('Latitude')

Biscay_Gulf_map_custom_whitout <-  read_sf("Biscay_Gulf_map.shp") %>% 
  ggplot() +
  geom_sf(data = Biscay_Gulf_map) +  
  # Ajout d'une échelle de taille
  #annotation_scale(location = "br", width_hint = 0.2) +
  # Ajout d'une flèche pour indiquer le nord
  #annotation_north_arrow(location = "tl", which_north = "true", 
  #                       pad_x = unit(0.05, "in"), pad_y = unit(0.05, "in"),
  #                       style = north_arrow_fancy_orienteering) + 
  theme(
    # Couleur du fond
    panel.background = element_rect(fill = "white", colour = "black"),
    # Couleur des lignes de grille
    panel.grid.major = element_line(colour = "transparent"),
    # Couleur du fond du graphique
    plot.background = element_rect(fill = "white", colour = "white"),
    # Couleur du fond de la légende
    legend.background = element_rect(fill = "white", colour = "white"),
    # Position de la légende
    legend.position = "bottom"
  ) +
  xlab('Longitude') +
  ylab('Latitude')


```

## Continuous value representation

```{r include=TRUE, echo=FALSE, warning=FALSE}

Mean_continuous_plot <- Biscay_Gulf_map_custom +
  geom_sf(data = S_x_df_poly, aes(fill = S_x, color = "gray")) +
  scale_fill_gradient(low = "#e9c46a", high = "#e76f51") +
   scale_color_manual(values = "white") +  
  guides(color = guide_none()) +
  theme(legend.position = "right") 
Mean_continuous_plot + geom_sf(data = Biscay_Gulf_map)

```

```{r include=TRUE, echo=TRUE, warning=FALSE}

S_x_df_poly_export <- S_x_df_poly %>% dplyr::select(S_x, geometry)

S_x_df_poly_export %>% sf::st_write(here("Solea_solea_Mean_continuous_plot.shp"), delete_dsn = TRUE, quiet = TRUE) 


```


<br>
<br>

## Clustering (discrete) value representation

<br>
<br>

```{r include=TRUE, echo=FALSE, warning=FALSE}

var_spatial_clust <- "S_x"
var_to_group <- c("year", "month")

### run clustering

list_spatial_clust_month_species  <- S_x_df_poly %>%
  group_by(across(all_of(var_to_group))) %>%
  group_split() %>%
  purrr::map(~ f_spatial_clust(df_spatial_clust = .x,
                               spatial_grid = loc_poly_sf_2,
                               var_spatial_clust = var_spatial_clust,
                               n_cluster = 3,
                               range_alpha = seq(0, 1, 0.1)))


New_S_x_df <- list_spatial_clust_month_species[[1]]$df_spatial %>% 
   mutate(cluster_order = as.character(as.integer(cluster)))
 
 med_clus_1 <- New_S_x_df %>% dplyr::filter(cluster_order == 1) %>% pull(S_x) %>% median()
 
 df_S_x <- New_S_x_df %>% mutate(cluster_med = case_when(
   cluster_order == 2 ~ "Medium",
   cluster_order == 3 ~ "High",
   cluster_order == 1 & S_x <= med_clus_1 ~ "Low",
   cluster_order == 1 & S_x > med_clus_1 ~ "Medium"
 ))

color_fill_map <- c( "Low" = "#e9c46a", "Medium" = "#f4a261", "High" ="#e76f51")
 
Mean_cluster_plot <- Biscay_Gulf_map_custom +
  geom_sf(data = df_S_x, aes(fill = cluster_med)) +
    scale_fill_manual(values = color_fill_map) +
   scale_color_manual(values = "gray80") +  
  theme(legend.position = "right") + labs(fill = "Cluster Type")

Mean_cluster_plot + geom_sf(data = Biscay_Gulf_map)


```


```{r include=TRUE, echo=TRUE, warning=FALSE}

df_S_x_export <- df_S_x %>% dplyr::select(S_x, cluster_med, geometry)

df_S_x_export %>% sf::st_write(here("Solea_solea_Mean_cluster_plot.shp"), delete_dsn = TRUE, quiet = TRUE) 


```

<br>
<br>

* Mean S_x value by cluster

```{r include=TRUE, echo=FALSE, warning=FALSE}

ratio <- df_S_x %>% 
  sf::st_drop_geometry() %>% 
  group_by(cluster_med) %>% 
  summarise(S_x_mean = mean(S_x)) %>% 
  mutate(cluster_med = factor(cluster_med, levels = c("Low", "Medium", "High"))) %>% 
  arrange(cluster_med) %>%
  rename(Cluster = cluster_med, Mean_S_x = S_x_mean)


```


```{r results = "asis"}

kable(ratio, caption = "Table: Mean S_x value by cluster")

```



# Month maps *Solea solea*

<br>

n = 11 years, from 2008 to 2018

<br>

```{r include=FALSE, echo=FALSE, warning=FALSE}

species_name <- "Solea solea"

year_vec <- c(2008:2018)

month_vec <- (1:12)

source("R/load_data/load_algave_model_output.R")

# Ici changer S_x_df

ab_df <- S_x_df %>% group_by(year, month, t) %>% summarise(Ab = sum(S_x))
S_x_df <- inner_join(S_x_df, ab_df)

S_x_df$year <- as.numeric(as.character(S_x_df$year))
S_x_df$month <- as.integer(as.character(S_x_df$month))

S_x_df <- S_x_df %>% mutate(month = sprintf("%02d", month)) %>%
 mutate(Year_Month = paste0(year,"_",month)) %>% 
   mutate(month = month  %>% as.numeric) %>%
 mutate(month = month  %>% as.factor() %>% factor(levels = c(1:12)))

# Maintenant le script fonctionne

# Ici changer S_x_df
S_x_df_mean <- S_x_df %>% group_by(key, month) %>% summarise(S_x = mean(S_x), Ab = mean(Ab))
S_x_df_unique_key <- S_x_df %>% as.data.frame() %>% dplyr::select(-c("month", "year", "t", "S_x", "Year_Month", "Ab")) %>% unique()
S_x_df_new_table <- left_join(S_x_df_unique_key, S_x_df_mean, by = "key")
# Erreur plus loin il faut rajouter une colonne "year"
S_x_df_new_table$year <- 1

S_x_df <- S_x_df_new_table

source("R/load_data/filter_spatial_grid.R")

source("R/load_data/join_model_spatial_grid_16.R")


var_spatial_clust <- "S_x"
var_to_group <- c("year", "month")

### run clustering

list_spatial_clust_month_species  <- S_x_df_poly %>%
  group_by(across(all_of(var_to_group))) %>%
  group_split() %>%
  purrr::map(~ f_spatial_clust(df_spatial_clust = .x,
                               spatial_grid = loc_poly_sf_2,
                               var_spatial_clust = var_spatial_clust,
                               n_cluster = 3,
                               range_alpha = seq(0, 1, 0.1)))


```


```{r include = TRUE, echo = FALSE, warning=FALSE}

 N = 12
 list_ggplot_median <- vector("list", N)
 for (i in 1:N) {
   
   New_S_x_df <- list_spatial_clust_month_species[[i]]$df_spatial %>% 
     mutate(cluster_order = as.character(as.integer(cluster)))
   
   med_clus_1 <- New_S_x_df %>% dplyr::filter(cluster_order == 1) %>% pull(S_x) %>% median()
   
   df_S_x <- New_S_x_df %>% mutate(cluster_med = case_when(
     cluster_order == 2 ~ "Medium",
     cluster_order == 3 ~ "High",
     cluster_order == 1 & S_x <= med_clus_1 ~ "Low",
     cluster_order == 1 & S_x > med_clus_1 ~ "Medium"
   ))
   
color_fill_map <- c( "Low" = "#e9c46a", "Medium" = "#f4a261", "High" ="#e76f51")
month_title <- paste("Month_", unique(list_spatial_clust_month_species[[i]]$plot_cluster$data$month), sep = "")


   list_ggplot_median[[i]] <- Biscay_Gulf_map_custom_whitout + 
     geom_sf(data = df_S_x, aes(fill = cluster_med), color = "transparent") +
     scale_fill_manual(values = color_fill_map, guide = "none") +
     ggtitle(month_title)+
     theme_void() + geom_sf(data = Biscay_Gulf_map)
   }

do.call("grid.arrange", c(list_ggplot_median, ncol = 4))


 

```

```{r include=TRUE, echo=TRUE, warning=FALSE}

N <- 12
for (i in 1:N) {

  filename <- paste0("Solea_solea_plot_month_", unique(list_spatial_clust_month_species[[i]]$plot_cluster$data$month), ".shp")
  
  df_S_x_export <- df_S_x %>% dplyr::select(S_x, month, cluster_med, geometry)
  
  sf::st_write(df_S_x_export, filename, delete_dsn = TRUE, quiet = TRUE)
  
}


```


* Cluster stability: Percentage of cluster attribution for each month between the 3 clusters (Low - Medium - High) 

```{r include=TRUE, echo=FALSE, warning=FALSE}

library(rlist)
library(knitr)

N = 12
tab_fin_var <- vector("list", N)
for (i in 1:N) {
  
  New_S_x_df <- list_spatial_clust_month_species[[i]]$df_spatial %>% 
    mutate(cluster_order = as.character(as.integer(cluster)))
  
  med_clus_1 <- New_S_x_df %>% dplyr::filter(cluster_order == 1) %>% pull(S_x) %>% median()
  
  tab_fin_var[[i]] <- New_S_x_df %>% mutate(cluster_med = case_when(
    cluster_order == 2 ~ "2",
    cluster_order == 3 ~ "2",
    cluster_order == 1 & S_x <= med_clus_1 ~ "1",
    cluster_order == 1 & S_x > med_clus_1 ~ "2"
  )) %>%
    dplyr::select(layer, cluster, cluster_med) %>% sf::st_drop_geometry() %>% 
    mutate(cluster= as.numeric(cluster)) %>% mutate(cluster_med = as.numeric(cluster_med)) %>%
    rename_with(function(x) paste(x, i, sep="_"), starts_with("cluster")) %>%
    arrange(layer)
}


tab_fin_var_cbind <- list.cbind(tab_fin_var) %>% dplyr::select(-layer) 

toto_matrix <- matrix(1, nrow = 12, ncol = 12)
toto_matrix_med <- matrix(1, nrow = 12, ncol = 12)

toto_matrix_cor <- matrix(1, nrow = 12, ncol = 12)
toto_matrix_med_cor <- matrix(1, nrow = 12, ncol = 12)

combinaison_2_a_2 <-combn(1:12, 2)

for (i in 1:ncol(combinaison_2_a_2)) {
  
  ind1 <- combinaison_2_a_2[1, i]
  ind2 <- combinaison_2_a_2[2, i]
  ind1_cor <- combinaison_2_a_2[1, i]
  ind2_cor <- combinaison_2_a_2[2, i]
  
  obj1 <- tab_fin_var_cbind[,paste("cluster", ind1, sep = "_")]
  obj2 <- tab_fin_var_cbind[,paste("cluster", ind2, sep = "_")]
  correla <- sum(obj1 == obj2)/length(obj1)
  
  obj1_med <- tab_fin_var_cbind[,paste("cluster_med", ind1, sep = "_")]
  obj2_med <- tab_fin_var_cbind[,paste("cluster_med", ind2, sep = "_")]
  correla_med <- sum(obj1_med == obj2_med)/length(obj2_med)
  
  
  obj1_cor <- tab_fin_var_cbind[,paste("cluster", ind1_cor, sep = "_")]
  obj2_cor <- tab_fin_var_cbind[,paste("cluster", ind2_cor, sep = "_")]
  correla_cor <- cor(obj1_cor, obj2_cor)
  
  obj1_med_cor <- tab_fin_var_cbind[,paste("cluster_med", ind1_cor, sep = "_")]
  obj2_med_cor <- tab_fin_var_cbind[,paste("cluster_med", ind2_cor, sep = "_")]
  correla_med_cor <- cor(obj1_med_cor, obj2_med_cor)
  

  toto_matrix[ind1, ind2] <-  correla
  toto_matrix[ind2, ind1] <-  correla
  
  toto_matrix_med[ind1, ind2] <-  correla_med
  toto_matrix_med[ind2, ind1] <-  correla_med
  
  
  toto_matrix_cor[ind1, ind2] <-  correla_cor
  toto_matrix_cor[ind2, ind1] <-  correla_cor
  
  toto_matrix_med_cor[ind1, ind2] <-  correla_med_cor
  toto_matrix_med_cor[ind2, ind1] <-  correla_med_cor

}

colnames(toto_matrix) <- month.name
rownames(toto_matrix) <- month.name

colnames(toto_matrix_med) <- month.name
rownames(toto_matrix_med) <- month.name

toto_matrix <- round(toto_matrix*100, 0)
toto_matrix_med <- round(toto_matrix_med*100, 0)

colnames(toto_matrix_cor) <- month.name
rownames(toto_matrix_cor) <- month.name

colnames(toto_matrix_med_cor) <- month.name
rownames(toto_matrix_med_cor) <- month.name


toto_matrix_cor <- round(toto_matrix_cor, 2)
toto_matrix_med_cor <- round(toto_matrix_med_cor, 2)


```


```{r results = "asis"}

kable(toto_matrix, caption = "Matrix of the percentage variation in cluster attribution, reflecting the stability of clusters between months")


      
```


```{r results = "asis", eval=FALSE}

#* Variation of cluster attribution for each month presence/absence (no cells attribution versus Low-Medium-High attribution)
#kable(toto_matrix_med, caption = " % presence/absence attribution (between cluster 0 and Low - Medium - High")

```


<br>
<br>

# Year maps *Solea solea*

<br>

n = 12 months, from January to December

<br>


```{r include=FALSE, echo=FALSE, warning=FALSE}

species_name <- "Solea solea"

year_vec <- c(2008:2018)

month_vec <- (1:12)

source("R/load_data/load_algave_model_output.R")

source("R/load_data/make_algave_spatial_grid_16.R")

# Ici changer S_x_df

ab_df <- S_x_df %>% group_by(year, month, t) %>% summarise(Ab = sum(S_x))
S_x_df <- inner_join(S_x_df, ab_df)

S_x_df$year <- as.numeric(as.character(S_x_df$year))
S_x_df$month <- as.integer(as.character(S_x_df$month))

S_x_df <- S_x_df %>% mutate(month = sprintf("%02d", month)) %>%
 mutate(Year_Month = paste0(year,"_",month)) %>% 
   mutate(month = month  %>% as.numeric) %>%
 mutate(month = month  %>% as.factor() %>% factor(levels = c(1:12)))

# Maintenant le script fonctionne

# Ici changer S_x_df
S_x_df_mean <- S_x_df %>% group_by(key, year) %>% summarise(S_x = mean(S_x), Ab = mean(Ab))
S_x_df_unique_key <- S_x_df %>% as.data.frame() %>% dplyr::select(-c("month", "year", "t", "S_x", "Year_Month", "Ab")) %>% unique()
S_x_df_new_table <- left_join(S_x_df_unique_key, S_x_df_mean, by = "key")
# Erreur plus loin il faut rajouter une colonne "month"
S_x_df_new_table$month <- 1

S_x_df <- S_x_df_new_table

source("R/load_data/filter_spatial_grid.R")

source("R/load_data/join_model_spatial_grid_16.R")

source("R/fun/project/f_spatial_clust.R")

var_spatial_clust <- "S_x"
var_to_group <- c("year", "month")

### run clustering

list_spatial_clust_month_species  <- S_x_df_poly %>%
  group_by(across(all_of(var_to_group))) %>%
  group_split() %>%
  purrr::map(~ f_spatial_clust(df_spatial_clust = .x,
                               spatial_grid = loc_poly_sf_2,
                               var_spatial_clust = var_spatial_clust,
                               n_cluster = 3,
                               range_alpha = seq(0, 1, 0.1)))


```

```{r include = TRUE, echo = FALSE, warning=FALSE}

 N = 11
 list_ggplot_median <- vector("list", N)
 for (i in 1:N) {
   
   New_S_x_df <- list_spatial_clust_month_species[[i]]$df_spatial %>% 
     mutate(cluster_order = as.character(as.integer(cluster)))
   
   med_clus_1 <- New_S_x_df %>% dplyr::filter(cluster_order == 1) %>% pull(S_x) %>% median()
   
   df_S_x <- New_S_x_df %>% mutate(cluster_med = case_when(
     cluster_order == 2 ~ "Medium",
     cluster_order == 3 ~ "High",
     cluster_order == 1 & S_x <= med_clus_1 ~ "Low",
     cluster_order == 1 & S_x > med_clus_1 ~ "Medium"
   ))
   
color_fill_map <- c( "Low" = "#e9c46a", "Medium" = "#f4a261", "High" ="#e76f51")
year_title <- paste("Year_", unique(list_spatial_clust_month_species[[i]]$plot_cluster$data$year), sep = "")

   list_ggplot_median[[i]] <- Biscay_Gulf_map_custom_whitout + 
     geom_sf(data = df_S_x, aes(fill = cluster_med), color = "transparent") +
     scale_fill_manual(values = color_fill_map, guide = "none") +
     ggtitle(year_title)+
     theme_void() + geom_sf(data = Biscay_Gulf_map)
   }

do.call("grid.arrange", c(list_ggplot_median, ncol = 4))


```

```{r include=TRUE, echo=TRUE, warning=FALSE}

N <- 11
for (i in 1:N) {

  filename <- paste0("Solea_solea_plot_year_", unique(list_spatial_clust_month_species[[i]]$plot_cluster$data$year), ".shp")
  
  df_S_x_export <- df_S_x %>% dplyr::select(S_x, year, cluster_med, geometry)
  
  sf::st_write(df_S_x_export, filename, delete_dsn = TRUE, quiet = TRUE)
  
}


```


* Cluster stability: Percentage of cluster attribution for each year between the 3 clusters (Low - Medium - High) 

```{r include=TRUE, echo=FALSE, warning=FALSE}

N = 11
tab_fin_var <- vector("list", N)
for (i in 1:N) {
  
  New_S_x_df <- list_spatial_clust_month_species[[i]]$df_spatial %>% 
    mutate(cluster_order = as.character(as.integer(cluster)))
  
  med_clus_1 <- New_S_x_df %>% dplyr::filter(cluster_order == 1) %>% pull(S_x) %>% median()
  
  tab_fin_var[[i]] <- New_S_x_df %>% mutate(cluster_med = case_when(
    cluster_order == 2 ~ "2",
    cluster_order == 3 ~ "2",
    cluster_order == 1 & S_x <= med_clus_1 ~ "1",
    cluster_order == 1 & S_x > med_clus_1 ~ "2"
  )) %>%
    dplyr::select(layer, cluster, cluster_med) %>% sf::st_drop_geometry() %>% 
    mutate(cluster= as.numeric(cluster)) %>% mutate(cluster_med = as.numeric(cluster_med)) %>%
    rename_with(function(x) paste(x, i, sep="_"), starts_with("cluster")) %>%
    arrange(layer)
}

tab_fin_var_cbind <- list.cbind(tab_fin_var) %>% dplyr::select(-layer) 

toto_matrix <- matrix(1, nrow = 11, ncol = 11)
toto_matrix_med <- matrix(1, nrow = 11, ncol = 11)

toto_matrix_cor <- matrix(1, nrow = 11, ncol = 11)
toto_matrix_med_cor <- matrix(1, nrow = 11, ncol = 11)

combinaison_2_a_2 <-combn(1:11, 2)

for (i in 1:ncol(combinaison_2_a_2)) {
  
  ind1 <- combinaison_2_a_2[1, i]
  ind2 <- combinaison_2_a_2[2, i]
  ind1_cor <- combinaison_2_a_2[1, i]
  ind2_cor <- combinaison_2_a_2[2, i]
  
  obj1 <- tab_fin_var_cbind[,paste("cluster", ind1, sep = "_")]
  obj2 <- tab_fin_var_cbind[,paste("cluster", ind2, sep = "_")]
  correla <- sum(obj1 == obj2)/length(obj1)
  
  obj1_med <- tab_fin_var_cbind[,paste("cluster_med", ind1, sep = "_")]
  obj2_med <- tab_fin_var_cbind[,paste("cluster_med", ind2, sep = "_")]
  correla_med <- sum(obj1_med == obj2_med)/length(obj2_med)
  
  obj1_cor <- tab_fin_var_cbind[,paste("cluster", ind1_cor, sep = "_")]
  obj2_cor <- tab_fin_var_cbind[,paste("cluster", ind2_cor, sep = "_")]
  correla_cor <- cor(obj1_cor, obj2_cor)
  
  obj1_med_cor <- tab_fin_var_cbind[,paste("cluster_med", ind1_cor, sep = "_")]
  obj2_med_cor <- tab_fin_var_cbind[,paste("cluster_med", ind2_cor, sep = "_")]
  correla_med_cor <- cor(obj1_med_cor, obj2_med_cor)

  toto_matrix[ind1, ind2] <-  correla
  toto_matrix[ind2, ind1] <-  correla
  
  toto_matrix_med[ind1, ind2] <-  correla_med
  toto_matrix_med[ind2, ind1] <-  correla_med
  
  toto_matrix_cor[ind1, ind2] <-  correla_cor
  toto_matrix_cor[ind2, ind1] <-  correla_cor
  
  toto_matrix_med_cor[ind1, ind2] <-  correla_med_cor
  toto_matrix_med_cor[ind2, ind1] <-  correla_med_cor

}

colnames(toto_matrix) <- c(2008:2018)
rownames(toto_matrix) <- c(2008:2018)

colnames(toto_matrix_med) <- c(2008:2018)
rownames(toto_matrix_med) <- c(2008:2018)

toto_matrix <- round(toto_matrix*100, 0)
toto_matrix_med <- round(toto_matrix_med*100, 0)

colnames(toto_matrix_cor) <- c(2008:2018)
rownames(toto_matrix_cor) <- c(2008:2018)

colnames(toto_matrix_med_cor) <- c(2008:2018)
rownames(toto_matrix_med_cor) <- c(2008:2018)

toto_matrix_cor <- round(toto_matrix_cor, 2)
toto_matrix_med_cor <- round(toto_matrix_med_cor, 2)

```


```{r results = "asis"}

kable(toto_matrix, caption = "Matrix of the percentage variation in cluster attribution, reflecting the stability of clusters between years")

```


```{r eval=FALSE}

#* Variation of cluster attribution for each year presence/absence no cells attribution versus Low-Medium-High attribution

#kable(toto_matrix_med, caption = " % presence/absence attribution (between cluster 0 and Low - Medium - High")
      
```


<br>
<br>

# Month x Year maps *Solea solea* {.tabset}

<br>
<br>

```{r include=FALSE, echo=FALSE, warning=FALSE}


species_name <- "Solea solea"

year_vec <- c(2008:2018)

month_vec <- (1:12)

source("R/fun/boot.R")

source("R/load_data/load_algave_model_output.R")

source("R/load_data/make_algave_spatial_grid_16.R")

# Ici changer S_x_df

ab_df <- S_x_df %>% group_by(year, month, t) %>% summarise(Ab = sum(S_x))
S_x_df <- inner_join(S_x_df, ab_df)

S_x_df$year <- as.numeric(as.character(S_x_df$year))
S_x_df$month <- as.integer(as.character(S_x_df$month))

S_x_df <- S_x_df %>% mutate(month = sprintf("%02d", month)) %>%
 mutate(Year_Month = paste0(year,"_",month)) %>% 
   mutate(month = month  %>% as.numeric) %>%
 mutate(month = month  %>% as.factor() %>% factor(levels = c(1:12)))

# Maintenant le script fonctionne

S_x_df <- S_x_df %>% dplyr::select(key, x, y, S_x, Ab, month, year, Year_Month)


source("R/load_data/filter_spatial_grid.R")

source("R/load_data/join_model_spatial_grid_16.R")

source("R/fun/project/f_spatial_clust.R")


var_spatial_clust <- "S_x"
var_to_group <- c("year", "month")

### run clustering

list_spatial_clust_month_species  <- S_x_df_poly %>%
  group_by(across(all_of(var_to_group))) %>%
  group_split() %>%
  purrr::map(~ f_spatial_clust(df_spatial_clust = .x,
                               spatial_grid = loc_poly_sf_2,
                               var_spatial_clust = var_spatial_clust,
                               n_cluster = 3,
                               range_alpha = seq(0, 1, 0.1)))

```


```{r include = TRUE, echo = FALSE, warning=FALSE}


N = 132
list_ggplot_med <- vector("list", N)
list_name_med <- vector("list", N)

for (i in 1:N) {
  
  list_name_med[[i]] <- list_spatial_clust_month_species[[i]]$df_spatial %>% 
    mutate(Year_Month = paste("Month", month, "Years", year, sep = " ")) %>% pull(Year_Month) %>%
    unique()
  
  
  New_S_x_df <- list_spatial_clust_month_species[[i]]$df_spatial %>% 
    mutate(cluster_order = as.character(as.integer(cluster)))
  
  med_clus_1 <- New_S_x_df %>% dplyr::filter(cluster_order == 1) %>% pull(S_x) %>% median()
  
 df_S_x <- New_S_x_df %>% mutate(cluster_med = case_when(
     cluster_order == 2 ~ "Medium",
     cluster_order == 3 ~ "High",
     cluster_order == 1 & S_x <= med_clus_1 ~ "Low",
     cluster_order == 1 & S_x > med_clus_1 ~ "Medium"
   ))
   
color_fill_map <- c( "Low" = "#e9c46a", "Medium" = "#f4a261", "High" ="#e76f51")
  
  list_ggplot_med[[i]] <-
    Biscay_Gulf_map_custom_whitout + 
    geom_sf(data = df_S_x, aes(fill = cluster_med), color = "transparent") +
    scale_fill_manual(values = color_fill_map, guide = "none") +
    ggtitle(list_name_med[[i]]) +
    theme_void() + geom_sf(data = Biscay_Gulf_map)

}

list_ggplot_month_med <- lapply(1:12, function(x) vector("list", 11))

for (i in 1:11) {
  for (y in 1:12){
    list_ggplot_month_med[[y]][[i]] <- list_ggplot_med[[(i-1)*12+y]]
  }
}


```


## January

```{r include = TRUE, echo=FALSE, message = FALSE}

do.call("grid.arrange", list_ggplot_month_med[[1]])

```

## February

```{r include = TRUE, echo=FALSE, message = FALSE}

do.call("grid.arrange", list_ggplot_month_med[[2]])

```

## March

```{r include = TRUE, echo=FALSE, message = FALSE}

do.call("grid.arrange", list_ggplot_month_med[[3]])

```

## April

```{r include = TRUE, echo=FALSE, message = FALSE}

do.call("grid.arrange", list_ggplot_month_med[[4]])

```

## May

```{r include = TRUE, echo=FALSE, message = FALSE}

do.call("grid.arrange", list_ggplot_month_med[[5]])

```

## June

```{r include = TRUE, echo=FALSE, message = FALSE}

do.call("grid.arrange", list_ggplot_month_med[[6]])

```

## July

```{r include = TRUE, echo=FALSE, message = FALSE}

do.call("grid.arrange", list_ggplot_month_med[[7]])

```

## August

```{r include = TRUE, echo=FALSE, message = FALSE}

do.call("grid.arrange", list_ggplot_month_med[[8]])

```

## September

```{r include = TRUE, echo=FALSE, message = FALSE}

do.call("grid.arrange", list_ggplot_month_med[[9]])

```

## October

```{r include = TRUE, echo=FALSE, message = FALSE}

do.call("grid.arrange", list_ggplot_month_med[[10]])

```

## November

```{r include = TRUE, echo=FALSE, message = FALSE}

do.call("grid.arrange", list_ggplot_month_med[[11]])

```

## December

```{r include = TRUE, echo=FALSE, message = FALSE}

do.call("grid.arrange", list_ggplot_month_med[[12]])

```

<br>
<br>

# Year x Month maps *Solea solea* {.tabset}

<br>
<br>

```{r include = TRUE, echo = FALSE, warning=FALSE}

N <- 132
list_ggplot_med <- vector("list", N)
list_name_med <- vector("list", N)

for (i in 1:N) {
  
  list_name_med[[i]] <- list_spatial_clust_month_species[[i]]$df_spatial %>% 
    mutate(Year_Month = paste("Year", year, "Month", month,  sep = " ")) %>% pull(Year_Month) %>%
    unique()
  
  New_S_x_df <- list_spatial_clust_month_species[[i]]$df_spatial %>% 
    mutate(cluster_order = as.character(as.integer(cluster)))
  
  med_clus_1 <- New_S_x_df %>% dplyr::filter(cluster_order == 1) %>% pull(S_x) %>% median()
  
  df_S_x <- New_S_x_df %>% mutate(cluster_med = case_when(
    cluster_order == 2 ~ "Medium",
    cluster_order == 3 ~ "High",
    cluster_order == 1 & S_x <= med_clus_1 ~ "Low",
    cluster_order == 1 & S_x > med_clus_1 ~ "Medium"
  ))
  
  color_fill_map <- c("Low" = "#e9c46a", "Medium" = "#f4a261", "High" = "#e76f51")
  
  list_ggplot_med[[i]] <- Biscay_Gulf_map_custom_whitout + 
    geom_sf(data = df_S_x, aes(fill = cluster_med), color = "transparent") +
    scale_fill_manual(values = color_fill_map, guide = "none") +
    ggtitle(list_name_med[[i]]) +
    theme_void() + geom_sf(data = Biscay_Gulf_map)
}

list_gglot_month_year <- lapply(1:11, function(x) vector("list", 12))

for (y in 1:11) {
  for (i in 1:12){
    list_gglot_month_year[[y]][[i]] <- list_ggplot_med[[(y-1)*12+i]]
  }
}


```

## Year 2008

```{r include = TRUE, echo=FALSE, message = FALSE}

do.call("grid.arrange", list_gglot_month_year[[1]])

```

## Year 2009

```{r include = TRUE, echo=FALSE, message = FALSE}

do.call("grid.arrange", list_gglot_month_year[[2]])

```

## Year 2010

```{r include = TRUE, echo=FALSE, message = FALSE}

do.call("grid.arrange", list_gglot_month_year[[3]])

```

## Year 2011

```{r include = TRUE, echo=FALSE, message = FALSE}

do.call("grid.arrange", list_gglot_month_year[[4]])

```

## Year 2012

```{r include = TRUE, echo=FALSE, message = FALSE}

do.call("grid.arrange", list_gglot_month_year[[5]])

```

## Year 2013

```{r include = TRUE, echo=FALSE, message = FALSE}

do.call("grid.arrange", list_gglot_month_year[[6]])

```

## Year 2014

```{r include = TRUE, echo=FALSE, message = FALSE}

do.call("grid.arrange", list_gglot_month_year[[7]])

```

## Year 2015

```{r include = TRUE, echo=FALSE, message = FALSE}

do.call("grid.arrange", list_gglot_month_year[[8]])

```

## Year 2016

```{r include = TRUE, echo=FALSE, message = FALSE}

do.call("grid.arrange", list_gglot_month_year[[9]])

```

## Year 2017

```{r include = TRUE, echo=FALSE, message = FALSE}

do.call("grid.arrange", list_gglot_month_year[[10]])

```

## Year 2018

```{r include = TRUE, echo=FALSE, message = FALSE}

do.call("grid.arrange", list_gglot_month_year[[11]])

```


<br>
<br>

