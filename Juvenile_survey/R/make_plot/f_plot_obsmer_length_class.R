f_plot_obsmer_length_class <- function(df_data, spp){
  
  df_data <- df_data %>%
    filter(spp == spp)
  
  ggplot(df_data, aes(x = lenCls, fill = spp)) + 
    geom_histogram(binwidth = 10) +
    geom_vline(data = df_data, aes(xintercept = len_minimal_landings ),
               color = "red",
               size = 1) +
    geom_vline(data = df_data, aes(xintercept = len_maturity_L50 ),
               color = "#E69F00",
               size = 1) +
    facet_wrap(~catchCat) +
    
    xlab("Length class (mm)") +
    ylab("Number") +
    ggtitle(paste("Obsmer : ", df_data$spp), "Red = L_min catch; Pink = L50") +
    theme(legend.position = "none")
}
