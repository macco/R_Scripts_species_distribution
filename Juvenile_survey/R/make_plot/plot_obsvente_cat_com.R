

hist_obsvente_com_cat <- ggplot() +
  geom_histogram(data = df_obsvente_HL, aes(Length_class), binwidth = 10) +
  facet_wrap(Species ~ Comm_size_cat, scales = "free_y", ncol = 3) + 
  xlim(100,500) +
  xlab("Length class (mm)") +
  ylab("Number") +
  geom_vline(data = df_length_minimal_L50, aes(xintercept = len_minimal_landings ),
             color = "#E69F00",
             size = 1)


f_save_plot(plot = hist_obsvente_com_cat,
            file_name = "hist_obsvente_com_cat",
            path = path_figure_ceteau_sole,
            save = save_plot,
            width = 29,
            height = 21,
            device = 'pdf',
            units = 'cm')

print(hist_obsvente_com_cat)


saveRDS(hist_obsvente_com_cat,
        file = here(path_data_tidy_plots,"hist_obsvente_com_cat.rds"))

###-----------------------------------------------------------------------------
###

source(here("R", "make_plot", "f_plot_cat_com_species.R"))

df_obsvente_HL %>%
  split(.$Species) %>%
  purrr::map(f_plot_cat_com_species) -> hist_obsvente_com_cat 


map2(.x = hist_obsvente_com_cat,
    .y = names(hist_obsvente_com_cat),
  ~ f_save_plot(plot = .x,
              file_name = paste0("hist_obsvente_com_cat", .y),
              path = path_figure_macco,
              save = TRUE,
              width = 29,
              height = 21,
              device = 'pdf',
              units = 'cm'))



df_obsvente_HL %>%
  split(.$Species) %>%
  purrr::map(f_plot_cat_com_species_no_facet) -> hist_cat_com_no_facet 


map2(.x = hist_cat_com_no_facet,
     .y = names(hist_cat_com_no_facet),
     ~ f_save_plot(plot = .x,
                   file_name = paste0("hist_obsvente_com_cat_no_facet", .y),
                   path = path_figure_macco,
                   save = TRUE,
                   width = 29,
                   height = 21,
                   device = 'pdf',
                   units = 'cm'))

