source("R/make_plot/f_plot_obsmer_length_class.R")

###-----------------------------------------------------------------------------

macco_hist_obsmer_length <- ggplot() + 
  geom_histogram(data = df_obsmer, aes(x = lenCls, fill = maturity), bins = 50) +
  # geom_vline(data = df_lophi, aes(xintercept = min_lenCls)) +
  xlab("Length class") + ylab("Number of sampled individuals") +
  geom_vline(data = df_obsmer, aes(xintercept = len_minimal_landings ),
             color = "red",
             size = 1) +
  geom_vline(data = df_obsmer, aes(xintercept = len_maturity_L50 ),
             color = "#E69F99",
             size = 1) +
  ggtitle(paste("Obsmer", min(df_obsmer$year), "-", max(df_obsmer$year))) +
  facet_wrap(~spp, scales = "free", ncol = 2)

f_save_plot(plot = macco_hist_obsmer_length,
            file_name = "macco_hist_obsmer_length",
            path = path_figure_macco,
            save = TRUE,
            width = 29,
            height = 21,
            device = 'pdf',
            units = 'cm')

###-----------------------------------------------------------------------------
boxplot_obsvente_lenclass <- ggplot(df_obsmer,
                                    aes(x = catchCat, y = lenCls, fill = spp)) + 
  geom_boxplot() +
  xlab("Catch category") +
  ylab("Length class")


###-----------------------------------------------------------------------------
###
df_obsmer %>%
  split(.$spp) %>%
  purrr::map(f_plot_obsmer_length_class) -> hist_obsmer_length_class


map2(.x = hist_obsmer_length_class,
     .y = names(hist_obsmer_length_class),
     ~ f_save_plot(plot = .x,
                   file_name = paste0("hist_obsmer_length_class", .y),
                   path = path_figure_macco,
                   save = TRUE,
                   width = 29,
                   height = 21,
                   device = 'pdf',
                   units = 'cm'))

