###-----------------------------------------------------------------------------
### plot fishing time per month for every year (one plot per year with 12 months)
###-----------------------------------------------------------------------------

### load function
source("R/make_plot/f_map_vms_effort.R")

### make map for each year
map_vms_effort <- map(levels(df_vms_month$year),
                           ~ f_map_vms_effort(year = .x))

### make name for saving figure
map_vms_effort_names <- map(levels(df_vms_month$year),
                                 ~paste0("maps_effort_month_", .x))
### save figure
walk2(map_vms_effort_names,
      map_vms_effort,
      ~{
        f_save_plot(plot = print(.y),
                    file_name = .x,
                    path = path_figure,
                    save = save_plot,
                    width = 29,
                    height = 21,
                    device = 'pdf',
                    units = 'cm')})
