f_map_vms_effort <- function(year) {
  
  df_vms_month <- df_vms_month %>%
    filter(year == year)
  
  plot_empty_map +
    geom_sf(data = df_vms_month, aes(fill = fishing_time), size = 0.001) +
    scale_fill_viridis_c(
      # trans = "log",
      # limits = c(0.001, round(max(df_vms_month$quantity_kg)+51, digits = -2)),
      name = "Effort") +
    facet_wrap( ~ month, ncol = 3) +
    ggtitle(paste0("Fishing effort per month"))
}

f_map_vms_effort_log <- function(year) {
  
  df_vms_month <- df_vms_month %>%
    filter(year == year)
  
  plot_empty_map +
    geom_sf(data = df_vms_month, aes(fill = fishing_time_log), size = 0.001) +
    scale_fill_viridis_c(
      # trans = "log",
      # limits = c(0.001, round(max(df_vms_month$quantity_kg)+51, digits = -2)),
      name = "Effort") +
    facet_wrap( ~ month, ncol = 3) +
    ggtitle(paste0("Fishing effort per month on the log-scale"))
}