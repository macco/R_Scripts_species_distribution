###-----------------------------------------------------------------------------
sf_obsmer <- df_obsmer %>%
  group_by(spp, landCat, year, month, rect) %>%
  summarise(n_obs = n())

sf_obsmer <- left_join(sf_obsmer,
                       sf_ices_rec,
                       by = c("rect" = "ICESNAME")) %>%
  st_as_sf()
