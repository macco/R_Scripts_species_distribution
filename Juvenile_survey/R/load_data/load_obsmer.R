load(here(path_data_raw, "obsmer/base_obsmer_non_validee_a_partir_01012021_COST_12_06_21.Rdata"))

df_hh <- COST@hh %>%
  filter(area %in% c("27.8.a", "27.8.b", "27.8.c"))

###-----------------------------------------------------------------------------
### dplyr::select data in the BoB only
df_obsmer <- COST@hl %>%
  left_join(df_hh, .) %>%
  left_join(., COST@sl) %>%
  filter(spp %in% species_vec,
         !is.na(lenCls),
         # catchCat == "DIS",             
         foVal == "V",
         year >= 2000) %>%
  group_by(spp) %>%
  mutate(min_lenCls = min(lenCls, na.rm = TRUE),
         date = ymd(date),
         quarter = quarter(date),
         year = year(date),
         month = month(date)) %>%
  # filter(year > 2016) %>%
  # uncount(lenNum) %>%
  left_join(., df_length_minimal_L50, by = c("spp" = "Species")) %>%
  mutate(maturity = case_when(lenCls >=  len_maturity_L50 ~ "Mature",
                              lenCls < len_maturity_L50 ~ "Immature"),
         maturity = fct_explicit_na(maturity))

###-----------------------------------------------------------------------------
### Spatial feature data frame
sf_obsmer <- df_obsmer %>%
  st_as_sf(coords = c("lonFin", "latFin")) %>% 
  st_set_crs(4326)

###-----------------------------------------------------------------------------
### summarize data by species, catch type and length class
dfs_obsmer <- df_obsmer %>%
  group_by(spp, catchCat, year, month) %>%
  summarise(min_lenCls = min(lenCls, na.rm = TRUE),
            max_lenCls = max(lenCls, na.rm = TRUE),
            n_obs = sum(lenNum),
            Presence = 1)


###-----------------------------------------------------------------------------
### set df_obsemer as sf and count number of fish per observed maree
sf_obsmer_quarter <- df_obsmer %>%
  group_by(spp, lonFin, latFin, lenCls, maturity, year, quarter) %>%
  summarise(n_obs = sum(lenNum)) %>%
  st_as_sf(coords = c("lonFin", "latFin"), crs = 4326)  %>%
  distinct()

###-----------------------------------------------------------------------------
### Count number of year per quarter for each isis rectangles
dfs_isis_grid_quarter_year <- st_join(sf_isis_grid, sf_obsmer_quarter) %>%
  st_drop_geometry() %>%
  dplyr::select(quarter, year, square) %>%
  distinct() %>%
  group_by(quarter, square) %>%
  summarise(n_year = n())

###-----------------------------------------------------------------------------
df_obsmer_quarter_ices <- st_join(sf_ices_rec, sf_obsmer_quarter) %>%
  st_drop_geometry() %>%
  left_join(., dfs_isis_grid_quarter_year) %>%
  group_by(spp, maturity, ICESNAME_1, year, quarter, n_year) %>%
  summarise(n_obs = sum(n_obs),
            presence = 1) %>%
  group_by(spp, maturity, ICESNAME_1, quarter, n_year) %>%
  summarise(mean_n = mean(n_obs),
            mean_presence = sum(presence) / n_year) %>%
  distinct()

###-----------------------------------------------------------------------------
df_obsmer_quarter_isis <- st_join(sf_isis_grid, sf_obsmer_quarter) %>%
  st_drop_geometry() %>%
  left_join(., dfs_isis_grid_quarter_year) %>%
  group_by(spp, maturity, square, year, quarter, n_year) %>%
  summarise(n_obs = sum(n_obs),
            presence = 1) %>%
  group_by(spp, maturity, square, quarter, n_year) %>%
  summarise(mean_n = mean(n_obs),
            mean_presence = sum(presence) / n_year) %>%
  distinct()

sf_obsmer_quarter_isis <- left_join(df_obsmer_quarter_isis, sf_isis_grid) %>%
  filter(maturity   != "(Missing)") %>%
  mutate(Presence = "Presence") %>%
  st_as_sf()

saveRDS(sf_obsmer_quarter_isis, here(path_data_tidy,
                                     "obsmer", "sf_obsmer_quarter_isis.rds" ))


### extract quarter 
sf_isis_grid_obsmer <- dplyr::select(sf_obsmer_quarter_isis,
                              quarter, geometry) %>%
  distinct() 
saveRDS(sf_isis_grid_obsmer, here(path_data_tidy,
                                     "obsmer", "sf_isis_grid_obsmer.rds" ))


###-----------------------------------------------------------------------------
dfs_obsmer <- df_obsmer %>%
  group_by(spp, catchCat) %>%
  summarise(min_lenCls = min(lenCls, na.rm = TRUE),
            max_lenCls = max(lenCls, na.rm = TRUE),
            n_obs = sum(lenNum))
saveRDS(dfs_obsmer, here(path_data_tidy,
                         "obsmer", "dfs_obsmer_length_spp.rds" ))

