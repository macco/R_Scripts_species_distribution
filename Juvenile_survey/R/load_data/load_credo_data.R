################################################################################
### load credo data from WGBIE 2020 Atelier
################################################################################

###-----------------------------------------------------------------------------
### sampling data
load("data/raw/sole/atelier_WGBIE/CSrall.rdata")

### use credo function to make correction to CSr
CSr <- f_correction_CSr(CSr)


###-----------------------------------------------------------------------------
### filter data per year
###-----------------------------------------------------------------------------

### data frame trip code 
df_cs_tr <- CSr@tr %>%
  filter(year %in% vec_year) %>%
  rename(NAVS_COD = vslId) %>%
  mutate(across(c("NAVS_COD", "year", "trpCode", "harbour"), as.factor))
as_tibble() %>%
  droplevels()

###-----------------------------------------------------------------------------
### data frame ca 
df_cs_ca <- CSr@ca %>%
  filter(trpCode %in% df_cs_tr$trpCode) %>%
  mutate(across(c("year", "trpCode"), as.factor))
as_tibble()

###-----------------------------------------------------------------------------
### data frame sl 
df_cs_sl <- CSr@sl %>%
  filter(trpCode %in% df_cs_tr$trpCode) %>%
  mutate(across(c("year", "trpCode"), as.factor))
as_tibble()

###-----------------------------------------------------------------------------
### data frame hl
df_cs_hl <- CSr@hl %>%
  filter(trpCode %in% df_cs_tr$trpCode,
         catchCat == "LAN") %>%
  mutate(across(c("year", "trpCode"), as.factor))
as_tibble()
