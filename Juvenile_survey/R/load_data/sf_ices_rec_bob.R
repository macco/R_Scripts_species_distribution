###-----------------------------------------------------------------------------
### longitude and latitude limits of the region
lon_lim <- c(-6, 1)
lat_lim <-  c(43, 48)

source("R/load_data/sf_coast_line.R")
source("R/load_data/sf_ices_rectangle.R")


###-----------------------------------------------------------------------------
### load ices rectangle
if (file.exists(here(path_data_tidy, "spatial", "sf_ices_rec_bob.rds"))) {
  sf_ices_rec <- read_rds(here(path_data_tidy, "spatial", "sf_ices_rec_bob.rds"))
} else {
  
  sf_ices_rec <-  sf_ices_rec %>%
    st_difference(., st_combine(sf_coast_line)) %>%
    st_crop(.,
            xmin = min(lon_lim),
            xmax = max(lon_lim),
            ymin = min(lat_lim),
            ymax = max(lat_lim))
  
  sf_ices_rec <- sf_ices_rec %>%
    st_difference(., st_combine(sf_coast_line))
  
  saveRDS(sf_ices_rec, here(path_data_tidy, "spatial", "sf_ices_rec_bob.rds"))
}

### coordinates system applied to all maps
sf_coord_system <- coord_sf(crs = st_crs(sf_coast_line),
                            expand = TRUE,
                            xlim = lon_lim,
                            ylim = lat_lim)
