### from credo function corrbase, I just renamed it
# basic correction for CSr credo object

f_correction_CSr <- function(CSr){
  #validation obsmer
  CSr@hh$foVal[CSr@hh$sampType=="M"]<-"V"
  #lenCode in mm
  CSr@sl$lenCode<-"mm"
  #correction aggregation level
  CSr@hh$aggLev[CSr@hh$aggLev=="TRUE"]<-T
  #si subSampWt ou wt  = 0 alors NA
  CSr@sl$wt[CSr@sl$wt<=0]<-NA
  CSr@sl$subSampWt[CSr@sl$subSampWt<=0]<-NA
  #remove NA lenCls in CSr@hl
  CSr@hl<-CSr@hl%>%filter(is.finite(lenCls))
  return(CSr)
}