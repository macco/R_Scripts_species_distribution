df_hh <- COST@hh %>%
  filter(area %in% c("27.8.a", "27.8.b", "27.8.c"))

df_tr <- COST@tr

filter(df_tr, daysAtSea > 30)

###-----------------------------------------------------------------------------
### Select data in the BoB only
df_obsmer <- COST@hl %>%
  left_join(df_hh, .) %>%
  left_join(., df_tr) %>%
  filter(grepl("G", foCatEu6)) %>%
  select(trpCode, date, time, foDur, daysAtSea, foVal, foCatEu6) %>%
  mutate(Duree_jour = foDur / 60 / 24 ) %>%
  distinct()
  # filter(spp %in% species_vec,
  #        !is.na(lenCls),
  #        foVal == "V",
  #        # catchCat == "DIS",             
  #        year >= 2009) %>%
  # group_by(spp) %>%
  # mutate(min_lenCls = min(lenCls, na.rm = TRUE),
  #        date = ymd(date),
  #        quarter = quarter(date),
  #        year = year(date),
  #        month = month(date)) %>%
  # # filter(year > 2016) %>%
  # uncount(lenNum) %>%
  # left_join(., df_length_minimal_L50, by = c("spp" = "Species")) %>%
  # mutate(maturity = case_when(lenCls >=  len_maturity_L50 ~ "Mature",
  #                             lenCls < len_maturity_L50 ~ "Imature"),
  #        maturity = fct_explicit_na(maturity))

ggplot() + 
  geom_histogram(data = df_obsmer, aes(Duree_jour)) +
  facet_grid(~foVal)
  
