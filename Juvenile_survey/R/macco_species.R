species_vec <- c(
  "Lepidorhombus whiffiagonis",
  "Lepidorhombus boscii",
  "Lophius piscatorius",
  "Lophius budegassa",
  "Solea solea",
  "Merlangius merlangus",
  "Merluccius merluccius",
  "Nephrops norvegicus",
  "Raja clavata",
  "Leucoraja naevus"
)

path_figure_macco <- paste0(path_figure, "/macco")
dir.create(path_figure_macco)
