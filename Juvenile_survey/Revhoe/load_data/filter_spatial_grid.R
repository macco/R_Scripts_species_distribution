## Cross with point
loc_x_sf <- st_as_sf(S_x_df[,c("x","y")],coords = c("x","y"),crs="+proj=longlat +datum=WGS84")
loc_x_sf_2 <- loc_x_sf[st_intersects(loc_x_sf,gridpolygon_sf) %>% lengths > 0,]
loc_x_sf_2 <- st_join(loc_x_sf_2,gridpolygon_sf)

plot(gridpolygon_sf,main="Polygon")

plot(loc_x_sf_2,main="Join polygon and point data")

loc_poly_sf_2 <- gridpolygon_sf %>%
  filter(layer %in% loc_x_sf_2$layer)


plot(loc_poly_sf_2, main="Join polygon and point data")
