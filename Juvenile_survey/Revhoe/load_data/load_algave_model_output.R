################################################################################
### load species data
################################################################################

load(file = here("data", "raw", "algave_b",
                 str_replace(species_name, " ", "_"),
                 paste0("G_index_",
                        str_replace(species_name, " ", "_"),
                        "_standardise.RData")))

load(file = here("data", "raw", "algave_b", 
                 str_replace(species_name, " ", "_"),
                 paste0("S_x_df_" , 
                        str_replace(species_name, " ", "_"),
                        "_standardise.RData")))

S_x_df <- S_x_df %>%
  mutate(year = factor(year),
         month = factor(month),
         key = factor(key)) %>%
  group_by(year, month, key) %>%
  filter(year %in% year_vec,
         month %in% month_vec)

