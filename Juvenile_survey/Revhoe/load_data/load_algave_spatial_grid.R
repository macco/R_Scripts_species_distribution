

# Load point data
load(here("data", "raw", "algave_b", "loc_x.RData"))

# Coordinates of the Bay of Biscay
grid_xmin <- -6
grid_xmax <- 0
grid_ymin <- 42
grid_ymax <- 48 #  # 52 # 52.5
grid_limit <- extent(c(grid_xmin,grid_xmax,grid_ymin,grid_ymax))

# Build grid
grid <- raster(grid_limit)
res(grid) <- 0.05
crs(grid) <- "+proj=longlat +datum=WGS84"
gridpolygon <- rasterToPolygons(grid)
gridpolygon$layer <- c(1:length(gridpolygon$layer))
gridpolygon_sf <- st_as_sf(gridpolygon)

## Cross with point
loc_x_sf <- st_as_sf(loc_x[,c("x","y")],coords = c("x","y"),crs="+proj=longlat +datum=WGS84")
loc_x_sf_2 <- loc_x_sf[st_intersects(loc_x_sf,gridpolygon_sf) %>% lengths > 0,]
loc_x_sf_2 <- st_join(loc_x_sf_2,gridpolygon_sf)

plot(gridpolygon_sf,main="Polygon")

plot(loc_x_sf_2,main="Join polygon and point data")

loc_poly_sf_2 <- gridpolygon_sf %>%
  filter(layer %in% loc_x_sf_2$layer)


plot(loc_poly_sf_2, main="Join polygon and point data")
