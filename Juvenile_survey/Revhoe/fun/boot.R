################################################################################
### First R file source, load useful packages and function
################################################################################

options(install.packages.check.source = "yes")

###-----------------------------------------------------------------------------
#### Load required package, and install it if not
list_of_packages <- c( "tidyverse", "here", "lubridate", "remotes",
                       # plot
                       "grid", "scales", "gridExtra", "latex2exp", "GGally", "ggthemes", "gghighlight","ggpubr",
                       # plotting maps
                       "maps", "mapdata", "maptools",
                       # dynamic document
                       "knitr", "rmarkdown", "captioner", "bookdown", "pander", "flextable", 
                       # spatial packages
                       "sf", "ggsn", "raster", "sp",
                       "JBUtilities",
                       "conclust",
                       "ClustGeo") 

# remotes::install_github("chavent/ClustGeo", force = FALSE)
# remotes::install_github('JBLecomte/JBUtilities', force = FALSE)

#list_of_packages <- c(list_of_packages, "ClustGeo")

#JBUtilities::InstallPackages(list_of_packages)

lapply(list_of_packages, library, character.only = TRUE)

###-----------------------------------------------------------------------------
### Load control file
source(here("Revhoe/fun/make_path.R"))

###-----------------------------------------------------------------------------
### ggplot2 functions
###-----------------------------------------------------------------------------

### Sober plot theme
source(here("Revhoe/Fun/ggplot2/SimpleTheme_ggplot2.R"))

### Function to produce customs boxplot
source(here("Revhoe/Fun/ggplot2/ggplot2_summary_fct_boxplot.R"))

### Palette
source(here("Revhoe/Fun/ggplot2/my_palette.R"))

### Extract legend from a plot
source(here("Revhoe/Fun/ggplot2/extract_plot_legend.R"))

### Extract legend from a plot
source(here("Revhoe/Fun/ggplot2/print.plot.R"))

###-----------------------------------------------------------------------------
### Other functions
###-----------------------------------------------------------------------------

### Load the utilities functions and control file variables
source(here("Revhoe/Fun/Utils/utilities.R"))

### Unpack the object stored in a list
source(here("Revhoe/Fun/Utils/unpack.list.R"))

### Compute the 25% quantile
source(here("Revhoe/Fun/Utils/quantile25.R"))
source(here("Revhoe/Fun/Utils/quantile75.R"))

### Standardization of a variable
source(here("Revhoe/Fun/Utils/Cov_Std.R"))

### Cutting a variable
source(here("Revhoe/Fun/Utils/cut_continuous_variable.R"))

### List objects in R + print their size
source(here("Revhoe/Fun/Utils/list_objects_and_size.R"))

###-----------------------------------------------------------------------------
### ggplot2 functions
###-----------------------------------------------------------------------------
if (length(list.files(paste0(c("Revhoe/fun/ggplot2/")))) > 0) {
  invisible(purrr::map(paste0(c("Revhoe/fun/ggplot2/"),
                              list.files(c("Revhoe/fun/ggplot2"), ".R")), source))
}

###-----------------------------------------------------------------------------
### Other functions
###-----------------------------------------------------------------------------

if (length(list.files(paste0(c("Revhoe/fun/utils/")))) > 0) {
  invisible(purrr::map(paste0(c("Revhoe/fun/utils/"),
                              list.files(c("Revhoe/fun/utils"), ".R")), source))
}

###-----------------------------------------------------------------------------
### Project's functions
###-----------------------------------------------------------------------------
if (length(list.files(paste0(c("Revhoe/fun/project/")))) > 0) {
  invisible(purrr::map(paste0(c("Revhoe/fun/project/"),
                              list.files(c("Revhoe/fun/project"), ".R")), source))
}

