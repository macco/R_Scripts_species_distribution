f_spatial_clust <- function(df_spatial_clust,
                            spatial_grid,
                            var_spatial_clust, 
                            n_cluster = 5,
                            range_alpha = c(0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6,
                                            0.7, 0.8, 0.9, 1)) {
  require("ClustGeo")
  require("spdep")
  
  df_no_spatial_clust <- df_spatial_clust %>%
    sf::st_drop_geometry() %>%
    droplevels()
  
  ###---------------------------------------------------------------------------
  df_spatial <- left_join(spatial_grid, df_no_spatial_clust) %>%
    mutate(across(where(is.numeric), tidyr::replace_na, 0))
  
  df_no_spatial <- df_spatial %>%
    sf::st_drop_geometry() %>%
    droplevels()
  
  if ("month" %in% colnames(df_no_spatial)) {
    df_spatial$month <- unique(levels(df_spatial$month))
  }
  
  if ("espece_fao_cod" %in% colnames(df_no_spatial)) {
    df_spatial$espece_fao_cod <- unique(levels(df_spatial$espece_fao_cod))
  }
  
  if ("espcece_fao_lib" %in% colnames(df_no_spatial)) {
    df_spatial$espcece_fao_lib <- unique(levels(df_spatial$espcece_fao_lib))
  }
  
  ###---------------------------------------------------------------------------
  ### Change the partition to take neighborhood constraint into account
  #list of neighbors of each grid square
  list_neighborhood <- poly2nb(df_spatial)
  remove_grid_cells_no_neighbor <- which(card(list_neighborhood) == 0)
  
  df_spatial <- df_spatial %>%
    filter(!row_number() %in% remove_grid_cells_no_neighbor)
  
  df_no_spatial <- df_no_spatial %>%
    filter(!row_number() %in% remove_grid_cells_no_neighbor)
  
  list_neighborhood <- poly2nb(df_spatial)
  
  ###---------------------------------------------------------------------------
  ### Spatial distance
  ###---------------------------------------------------------------------------
  ### no spatial constraints
  matrix_dist_var <- df_no_spatial %>%
    dplyr::select(any_of(var_spatial_clust)) %>%
    dist(.)
  # tree <- hclustgeo(matrix_dist_var)
  
  
  matrix_neighborhood <- nb2mat(list_neighborhood, style = "B")
  diag(matrix_neighborhood) <- 1
  colnames(matrix_neighborhood) <- rownames(matrix_neighborhood) <- df_spatial$square
  
  # The dissimilarity matrix D1 is then 1 minus A.
  matrix_dist_neighborhood <- as.dist(1 - matrix_neighborhood)
  
  ###---------------------------------------------------------------------------
  ### Choice of the mixing parameter alpha
  ### The procedure for the choice of alpha is repeated here with the new matrix D1.
  
  alpha_choice <- choicealpha(matrix_dist_var,
                              matrix_dist_neighborhood,
                              range_alpha,
                              n_cluster,
                              graph = FALSE)
  
  df_alpha_choice <- data.frame(D0 = alpha_choice$Qnorm[,1],
                                D1 = alpha_choice$Qnorm[,2],
                                alpha = alpha_choice$range.alpha) %>%
    pivot_longer(., cols = starts_with("D"))
  
  
  plot_alpha_choice <- ggplot(df_alpha_choice,
                              aes(x = alpha, y =  value, color = name)) +
    geom_point() + 
    geom_line() +
    ylim(0,1) + ylab("QNorm")
  
  set_alpha <- range_alpha[which.min(abs(alpha_choice$Qnorm[,1] - 
                                           alpha_choice$Qnorm[,2]))]
  
  ### Modified partition obtained with alpha
  tree <- hclustgeo(matrix_dist_var,
                    matrix_dist_neighborhood,
                    alpha = set_alpha)
  tree_cluster <- cutree(tree, n_cluster)
  
  df_spatial <- df_spatial %>%
    mutate(cluster = as.factor(tree_cluster),
           cluster = fct_reorder(cluster,
                                 .x = .data[[var_spatial_clust]],
                                 .desc = FALSE))
  
  map_cluster <- ggplot() + 
    geom_sf(data = df_spatial, aes(fill = cluster))
  
  plot_cluster <- ggplot(data = df_spatial,
                         aes(x = cluster, y = .data[[var_spatial_clust]],
                             fill = cluster)) +
    stat_boxplot(geom = 'errorbar', width = 0.3) +
    #geom_jitter(position = position_jitter(0.2), alpha = 0.1, shape = 1) +
    geom_boxplot(width = 0.6) +
    stat_summary(fun.data = f_n_obs,
                 fun.args = list(scale = round(max(df_spatial[[var_spatial_clust]]),
                                               digits = -2)),
                 geom = "text",
                 # aes(group=cluster),
                 hjust = 0.5, position = position_dodge(0.6)) +
    expand_limits(y = 0) 
  
  
  return(list(df_spatial = df_spatial ,
              map_cluster = map_cluster,
              plot_cluster = plot_cluster,
              plot_alpha_choice = plot_alpha_choice))
}